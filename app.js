const http = require('http')

const hostname = 'localhost'
console.log(process)
const processArguments = process.argv.slice(2)?.reduce((acc, cur) => {
  const [arg, val] = cur.split('=')
  acc[arg.replace('--', '')] = val
  return acc
}, {})

const port = processArguments.port || 3000

let count = 0
const server = http.createServer((req, res) => {
  const message = JSON.stringify({
    message: 'Request handled successfully',
    requestCount: ++count
  })
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(message)
})

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})
